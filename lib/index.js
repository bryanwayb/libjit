var jit = require('../build/Release/jit.node');

function Assembly(exec) {
	var id = jit.assemble(exec);
	
	if(!id) {
		throw new Error('Unable to allocate JIT handle');
	}
	
	this.handle = id;
	
	this.execute = function(callback) {
		if(this.handle) {
			if(!jit.execute(this.handle, callback)) {
				throw new Error('There was an error finding the internal handle');
			}
		}
		else {
			throw new Error('Cannot execute a deallocated handle');
		}
	};
	
	this.executeSync = function() {
		if(this.handle) {
			if(!jit.executeSync(this.handle)) {
				throw new Error('There was an error finding the internal handle');
			}
		}
		else {
			throw new Error('Cannot execute a deallocated handle');
		}
	};
	
	this.release = function() {
		if(this.handle) {
			var handleId = this.handle;
			this.handle = 0;
			return jit.dealloc(handleId);
		}
		else {
			return false;
		}
	}
}

module.exports = {
	asm: function(exec) {
		return new Assembly(exec);
	}
};