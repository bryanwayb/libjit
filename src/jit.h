#ifndef __JIT_H
#define __JIT_H

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>

#define ALLOC_EXEC_PAGE(size) VirtualAlloc(0, size, MEM_COMMIT, PAGE_READWRITE)
#define PROTECT_EXEC_PAGE(ptr, size) { unsigned long tmpVar; VirtualProtect(ptr, size, PAGE_EXECUTE, &tmpVar); }
#define FREE_EXEC_PAGE(ptr, size) VirtualFree(ptr, size, MEM_RELEASE)

#else // Assuming here

#include <sys/mman.h>

#define ALLOC_EXEC_PAGE(size) mmap(NULL, size, PROT_WRITE | PROT_EXEC, MAP_ANON | MAP_PRIVATE, -1, 0)
#define PROTECT_EXEC_PAGE(ptr, size) mprotect(ptr, size, PROT_EXEC)
#define FREE_EXEC_PAGE(ptr, size) munmap(ptr, size)

#endif

struct ExecuteCallback
{
	uv_work_t request;
	void* handle;
	v8::Persistent<v8::Function> callback;
};

void assemble(const v8::FunctionCallbackInfo<v8::Value>&);
void executeSync(const v8::FunctionCallbackInfo<v8::Value>&);
void execute(const v8::FunctionCallbackInfo<v8::Value>&);
void executeCallback(uv_work_t*);
void executeCallbackAfter(uv_work_t*,int);
void dealloc(const v8::FunctionCallbackInfo<v8::Value>&);

struct HandleStore
{
	uint32_t id;
	void *handle;
	size_t size;
};

void* getHandle(uint32_t);
uint32_t addHandle(void*, size_t);
struct HandleStore* removeHandle(uint32_t);

#endif