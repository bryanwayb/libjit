#include <node.h>
#include <string.h>
#include <uv.h>
#include "jit.h"

#include <stdio.h>

void init(v8::Handle<v8::Object> exports)
{
	NODE_SET_METHOD(exports, "assemble", assemble);
	NODE_SET_METHOD(exports, "execute", execute);
	NODE_SET_METHOD(exports, "executeSync", executeSync);
	NODE_SET_METHOD(exports, "dealloc", dealloc);
}

static uint32_t handleLength = 0;
static struct HandleStore *handleStore = NULL;
static uint32_t handleNextId = 0;

void* getHandle(uint32_t handle)
{
	void *ret = NULL;
	
	if(handleLength && handleStore)
	{
		struct HandleStore *ptr = handleStore;
		for(uint32_t index = 0; index < handleLength; index++, ptr++)
		{
			if(ptr->id == handle)
			{
				ret = ptr->handle;
				break;
			}
		}
	}
	
	return ret;
}

uint32_t addHandle(void *handlePtr, size_t size)
{
	if(handleStore == NULL)
    {
        handleStore = (struct HandleStore*)malloc(sizeof(struct HandleStore));
    }
    else
    {
        handleStore = (struct HandleStore*)realloc(handleStore, sizeof(struct HandleStore) * (handleLength + 1));
    }
	
	struct HandleStore *ptr = handleStore + (handleLength++);
    
    ptr->id = ++handleNextId;
    ptr->handle = handlePtr;
	ptr->size = size;
    
    return ptr->id;
}

struct HandleStore* removeHandle(uint32_t handle)
{
	if(handleLength && handleStore)
    {
        HandleStore *ptr = handleStore;
        for(uint32_t i = 0; i < handleLength; i++, ptr++)
        {
            if(handle == ptr->id)
            {
                handleStore[i] = handleStore[--handleLength];
            	handleStore = (struct HandleStore*)realloc(handleStore, sizeof(struct HandleStore) * handleLength);
				return ptr;
            }
        }
    }
    
    return NULL;
}

void assemble(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::HandleScope scope(isolate);
	
	if(args.Length() > 0)
	{
		if(args[0]->IsArray())
		{
			v8::Handle<v8::Array> opcodeArray = v8::Handle<v8::Array>::Cast(args[0]);
			uint32_t len = opcodeArray->Length();
			uint32_t ret = 0;
			
			if(len > 0)
			{
				unsigned char *buf = (unsigned char*)ALLOC_EXEC_PAGE(len);
				uint32_t index = 0;
				v8::Local<v8::Context> context = isolate->GetCurrentContext();
				for(; index < len; index++)
				{
					v8::MaybeLocal<v8::Value> v = opcodeArray->Get(context, index);
					v8::Local<v8::Value> localV;
					if(v.IsEmpty() || !v.ToLocal(&localV) || !localV->IsNumber())
					{
						isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Invalid operation code entry")));
						break;
					}
					else
					{
						buf[index] = (unsigned char)(((int64_t)v8::Handle<v8::Integer>::Cast(localV)->Value()) & 0xFF);
					}
				}
				
				if(index == len)
				{
					ret = addHandle(buf, len);
					PROTECT_EXEC_PAGE(buf, len);
				}
				else
				{
					FREE_EXEC_PAGE(buf, len);
				}
			}
			
			args.GetReturnValue().Set(v8::Integer::New(isolate, ret));
		}
		else
        {
            isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Invalid parameter type, must be of type Array")));
        }
	}
	else
	{
		isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Must supply a operation code array")));
	}
}

void executeSync(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::HandleScope scope(isolate);
	
	if(args.Length() > 0)
	{
		if(args[0]->IsNumber())
		{
			uint32_t id = (uint32_t)v8::Handle<v8::Integer>::Cast(args[0])->Value();
			bool ret = false;
			
			void (*exec)() = (void (*)())getHandle(id);
			if(exec) {
				ret = true;
				exec();
			}
			
			args.GetReturnValue().Set(v8::Boolean::New(isolate, ret));
		}
		else
        {
            isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Invalid parameter type, must be of type number")));
        }
	}
	else
	{
		isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Must supply a handle ID")));
	}
}

void execute(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::HandleScope scope(isolate);
	
	if(args.Length() > 1)
	{
		if(args[0]->IsNumber())
		{
			if(args[1]->IsFunction())
			{
				uint32_t id = (uint32_t)v8::Handle<v8::Integer>::Cast(args[0])->Value();
				bool ret = false;
				
				void *execHandle = getHandle(id);
				if(execHandle) {
					ret = true;
					
					ExecuteCallback *async = new ExecuteCallback;
					async->request.data = async;
					v8::Handle<v8::Function> func = v8::Handle<v8::Function>::Cast(args[1]);
					async->callback.Reset(isolate, func);
					async->handle = getHandle(id);
					
					uv_queue_work(uv_default_loop(), &async->request, executeCallback, executeCallbackAfter);
				}
				
				args.GetReturnValue().Set(v8::Boolean::New(isolate, ret));
			}
			else
			{
				isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Invalid parameter type, must be a callback")));
			}
		}
		else
        {
            isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Invalid parameter type, must be of type number")));
        }
	}
	else
	{
		isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Must supply a handle ID and callback")));
	}
}

void executeCallback(uv_work_t *wt)
{
	ExecuteCallback *async = static_cast<ExecuteCallback*>(wt->data);
	void (*exec)() = (void (*)())async->handle;
	if(exec) {
		exec();
	}
}

void executeCallbackAfter(uv_work_t *wt , int status)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::HandleScope scope(isolate);
	
	ExecuteCallback *async = static_cast<ExecuteCallback*>(wt->data);
	
	v8::Local<v8::Function> func = v8::Local<v8::Function>::New(isolate, async->callback);
	func->Call(isolate->GetCurrentContext()->Global(), 0, NULL);

	delete async;
}

void dealloc(const v8::FunctionCallbackInfo<v8::Value>& args)
{
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	v8::HandleScope scope(isolate);
	
	if(args.Length() > 0)
	{
		if(args[0]->IsNumber())
		{
			uint32_t id = (uint32_t)v8::Handle<v8::Integer>::Cast(args[0])->Value();
			
			bool ret = false;
			
			struct HandleStore *ptr = removeHandle(id);
			
			if(ptr)
			{
				ret = true;
				FREE_EXEC_PAGE(ptr->handle, ptr->size);
			}
			
			args.GetReturnValue().Set(v8::Boolean::New(isolate, ret));
		}
		else
        {
            isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Invalid parameter type, must be of type number")));
        }
	}
	else
	{
		isolate->ThrowException(v8::Exception::Error(v8::String::NewFromUtf8(isolate, "Must supply a handle ID")));
	}
}

NODE_MODULE(addon, init)